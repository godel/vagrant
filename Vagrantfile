# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.require_version ">= 1.7.1"

# Variables.

# drupal_docroot should contain Drupal's index.php. Examples:
# You should not need to change this as of 7.x-3.x as the drupal docroot should
# always be the repository root, with subdirectories setup as symlinks.
# - '/' => Pantheon
# - '/docroot' => Acquia
# - '/app' => PreviousNext/Technocrat
drupal_docroot = '/'

facts    = {
  "drupal_docroot" => drupal_docroot,
}

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  ##
  # Configure the VM
  ##

  # Use apt-get caching if available
  # http://fgrehm.viewdocs.io/vagrant-cachier
  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
  end

  # http://www.vagrantbox.es/
  config.vm.box_url = "http://puppet-vagrant-boxes.puppetlabs.com/ubuntu-server-12042-x64-vbox4210-nocm.box"
  config.vm.box = "Puppetlabs Ubuntu 12.04.2 x86_64, VBox 4.2.10, No Puppet or Chef"

  # We want to map the guest's port 80 (http) to some port on the host.
  # config.vm.network :forwarded_port, host: 4567, guest: 80
   #Vagrant.configure("2") do |config|
    # If this causes errors about "failing to create the host-only adapter"
    # @see https://coderwall.com/p/ydma0q
    config.vm.network "private_network", ip: "192.168.50.5"
  # end
  config.vm.hostname = "vagrant.ld"

  # We want to forward our ssh agent so we can connect to remote servers more
  # easily from within the box.
  config.ssh.forward_agent = true

  # This updates /etc/hosts for us. For this to work, we need the hostsupdater
  # plugin to be installed - https://github.com/cogitatio/vagrant-hostsupdater
  # $ vagrant plugin install vagrant-hostsupdater
  config.hostsupdater.aliases = [
    "vagrant.ld",
    "www.vagrant.ld",
    # This is a hack. You can access the installer through install.ld simply
    # because it falls back to sites/default.
    "install.ld",
    "two.ld",
    "www.two.ld"
  ]

  # Share the docroot of our project as /drupal with the guest VM.
  config.vm.synced_folder "../" + drupal_docroot, "/drupal",
    type: "rsync",
    # Exclude Drupal domains managed through provisioning scripts from rsync.
    exclude: [
      "sites/default",
      "sites/vagrant.ld",
      "sites/two.ld",
      ".git",
      ".vagrant",
      "vagrant",
      "vendor"
    ],
    args: [
      '-rvlt',
      '--delete',
      '--progress'
    ],
    group: 'www-data',
    owner: 'www-data'

  # Share the docroot of our project 2-way as /dev with the guest VM.
  # The idea here is that Drupal can write to this directory when it needs to,
  # such as when exporting features.
  config.vm.synced_folder "../", "/drupal-dev",
    group: 'www-data',
    owner: 'www-data'

  config.vm.provider "virtualbox" do |v|
    v.memory = 2048
  end

  ###
  # Provisioning
  ###

  # Get puppet on the box.
  config.vm.provision :shell, :path => "scripts/puppet.sh"

  # Remove pesky "stdin not a tty" messages.
  config.vm.provision :shell, :path => "scripts/stdin_not_a_tty.sh"

  # User permissions.
  config.vm.provision :shell, :path => "scripts/permissions.sh"

  # Run any provisioning scripts that need to happen *before* the default
  # manifest. This is essentially limited to updating apt-get repositories and
  # bug fixes.
  config.vm.provision "puppet" do |puppet|
    puppet.manifests_path = "manifests"
    puppet.manifest_file = "pre.pp"
    puppet.module_path = "modules"
    puppet.hiera_config_path = "hiera.yaml"
    puppet.options = "--verbose --debug"
    puppet.facter = facts
  end

  # Allow Puppet to do its magic.
  config.vm.provision "puppet" do |puppet|
    puppet.manifests_path = "manifests"
    puppet.manifest_file = "default.pp"
    puppet.module_path = "modules"
    puppet.hiera_config_path = "hiera.yaml"
    puppet.options = "--verbose --debug"
    puppet.facter = facts
  end

  config.vm.provision :shell, :path => "scripts/drush-modules.sh"

end
