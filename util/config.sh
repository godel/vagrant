DRUPALDIR=/drupal;
CURRENTPATH=`pwd`;

# Allow all of this to be set per-project.
. $DRUPALDIR/config.sh;

###
# PROJECT VARIABLES
###

# This is the name of the remote drush alias, as it is known from INSIDE the
# vagrant box. SSH into vagrant and run `drush sa` if you are unsure what it is
# called. This is likely to need to be changed per-project.
if [ -z "$REMOTEALIAS" ]
  then
    REMOTEALIAS=@dev;
fi

# This is the name of the hosting provider. The following values are supported:
# acquia
# pantheon
# manual
if [ -z "$HOSTINGPROVIDER" ]
  then
    HOSTINGPROVIDER=acquia;
fi

# Whether or not we're using SOLR on this project.
if [ -z "$SOLR" ]
  then
    SOLR="SOLR";
fi

###
# TESTING VARIABLES
###

# This is the behat profile to use for testing. Supported values can be found in
# behat.yml.
if [ -z "$BEHATPROFILE" ]
  then
    BEHATPROFILE=default;
fi

# This is the path to the Code Sniffer standard to use.
if [ -z "$CSSTANDARD" ]
  then
    CSSTANDARD=/home/vagrant/.drush/coder/coder_sniffer/Drupal;
fi

# These are the file patterns for Code Sniffer to ignore.
if [ -z "$CSIGNORE" ]
  then
    CSIGNORE="";
    # Ignore features exports.
    CSIGNORE+="*.features.*"
    CSIGNORE+=",*.default_elysia_cron_rules.inc";
    # Ignore the boxsizing.php in the aurora subtheme.
    CSIGNORE+=",box-sizing/boxsizing.php";
    # Ignore oembed features exports.
    CSIGNORE+=",*.oembedcore.inc,*.oembed_provider.inc";
fi

# These are the extensions for Code Sniffer to sniff.
if [ -z "$CSEXTENSIONS" ]
  then
    CSEXTENSIONS=php,module,inc,install,test,profile,theme;
fi

###
# VAGRANT ENVIRONMENT VARIABLES
###

# This is the name of the local drush alias, as it is known from INSIDE the
# vagrant box. SSH into vagrant and run `cd /drupal; drush sa` if you are unsure
# what it is called. This should NOT need to be changed per-project.
if [ -z "$LOCALALIAS" ]
  then
    LOCALALIAS=vagrant.ld;
fi

# This is the URL that vagrant is accessible on from OUTSIDE vagrant. This
# should NOT need to be changed per-project.
if [ -z "$LOCALURL" ]
  then
    LOCALURL=http://vagrant.ld/;
fi

# This is the path that drupal sites are located at INSIDE vagrant. This should
# NOT need to be changed per-project.
if [ -z "$SITESDIR" ]
  then
    SITESDIR=$DRUPALDIR/sites;
fi

# This is the path that public files are located at INSIDE vagrant. This should
# NOT need to be changed per-project.
if [ -z "$LOCALFILESDIR" ]
  then
    LOCALFILESDIR=/drupal/sites/vagrant.ld/public/;
fi

# This is the user that we expect to be running this script. This should NOT
# need to be changed per-project. Is generally "vagrant" but might be "ubuntu"
# on drone.io.
if [ -z "$SAFEUSER" ]
  then
    SAFEUSER=vagrant;
fi
