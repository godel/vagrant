This repository is intended to be a portable Vagrant box that will work out of
the box for 90% of Drupal 7 non-multi-site projects and should be reasonably
easy to customise to accomodate anything more complex.

As of the 7.x-3.x branch of the default Drupal template at
https://bitbucket.org/godel/t7.x, this repository has branches named to match.
This means that for the maximum compatibility, if you built your project from
the 7.x-3.x branch in the template, use the 7.x-3.x branch in the Vagrant repo.

## Installation

The Vagrant docs are excellent (http://docs.vagrantup.com/v2/) but this primer
will get you up and running with a local environment quickly!

In Vagrant the Host machine is your computer and the Guest machine is the VM.

0. Download and install VirtualBox - Version 4.2
    * As of the time of writing, there was a bug in version 4.3 of VirtualBox.
    In the future this bug will be resolved and we we'll be able to upgrade.
    * VirtualBox is a reasonably good free tool for working with VMs.
    * https://www.virtualbox.org/

0. Download and install Vagrant - Version 1.6+
    * Vagrant is the software that parses our configuration and uses it to build
    a VM running our site for us.
    * It is important that you have Vagrant 1.6+ in order for our shared folders
    to work.
    * The Vagrantfile explicitly specifies version 1.6+ and will fatal if you
    don't have the right version.
    * http://www.vagrantup.com/

0. Install the vagrant-hostsupdater Vagrant plugin
    * This plugin will add and remove entries to your /etc/hosts file that
    correspond to different domains used by the site. The entries are added and
    removed when you start/build stop/destroy your Vagrant VM.
    * https://github.com/cogitatio/vagrant-hostsupdater
    * $ vagrant plugin install vagrant-hostsupdater

    __OR__ Install some other plugins to improve Vagrant.
    * Some examples are at the end of this README.
    * If nothing else, I *highly* recommend installing vagrant-cachier as it
    greatly improves build times.

0. Build the Vagrant VM
    * Navigate to the /vagrant directory within this repository
    * Run the command: $ vagrant up
    * The first time you build the VM will be the slowest as it will need to
    download a disk image of the operating system (700+ MB)
    * On subsequent runs, Vagrant will attempt to use a previously downloaded
    image if it exists, which is much faster
    * Once the image has downloaded, the full build process should take 5-10
    mins

0. Download and install Sequel Pro
    * Sequel Pro is a popular, free db management tool
    * http://www.sequelpro.com/
    * You need to set this up under the SSH tab
    * Name: vagrant@vagrant

      MySQL Host: `127.0.0.1`

      Username: `vagrant`

      Password: `vagrant`

      Database: *leave default*

      Port: *leave default*

      SSH Host: `127.0.0.1`

      SSH User: `vagrant`

      SSH Password: `vagrant`

      SSH Port: `2222`


## Do this during initial installation and periodically afterwards

0. Manually import the latest database backup into the `vagrant` database.

    __OR__ Install a new site if there is no existing database for this project.
    * Visit install.ld after a fresh provision.
    * Work through the Drupal interactive installer using the relevant database
    details from step 5.
    * Once the installer is complete, you can swap back to vagrant.ld to
    continue working.

    __OR__ hopefully there is a magical database and files rebase script
    available.
    * Generally this will live in the project's repository, rather than in the
    Vagrant repository.
    * This is actually the best option if it's available. Read the README file
    in the parent repo to see if this is available.

0. You should now be able to access the site locally at `http://vagrant.ld`

0. We are using rsync to keep our shared directories in sync, so start watching.
    * You can exclude directories from syncing in the Vagrantfile and also send
    other arguments to rsync as required.
    * https://github.com/mitchellh/vagrant/blob/master/website/docs/source/v2/synced-folders/rsync.html.md
    * `vagrant rsync-auto`

0. '/shared' in the vagrant repository is 2-way synced inside /drupal-dev
    * There is a .gitignore rule to stop anything in here getting committed.
    * Drop db dumps in here for easy drush access if that makes sense to you.
    * Export Features to `/drupal-dev/path/to/somewhere`

## Using Drush

Drush is installed inside the Vagrant VM but there's no special configuration
to make links between the host and guest machines. Specifically, there is a bug
in drush that prevents configuring aliases to work inside Vagrant when running
commands on the host machine:
@see http://drupal.stackexchange.com/questions/107339/how-do-you-write-a-drush-alias-for-a-local-vagrant-box
@see https://github.com/drush-ops/drush/pull/546

This has been fixed in the latest dev version of drush, but assuming you're not
using that, you just need to make creative use of `vagrant ssh`.

Example, run from the vagrant directory:

`vagrant ssh -c "cd /drupal; drush vagrant.ld cc all;"`

## Vagrant commands

Rather than duplicate documentation here, you should at least read up on the
command line tools for vagrant at http://docs.vagrantup.com/v2/cli/index.html

## Project commands

We have a few scripts that help us perform common development tasks.

These can be configured by creating a `config.sh` file in your project root.

The most likely bash variables you will want to set in `config.sh` are:

`REMOTEALIAS` = The remote alias of the development environment.

`HOSTINGPROVIDER` = Should be either `acquia` or `pantheon`.

`SOLR` = If anything other than the string `SOLR`, we won't try to do things
with Apache Solr in our scripts.

### The scripts:

Run these from the vagrant directory, outside Vagant.

- Rebase local from dev:
  `vagrant ssh -c ". /vagrant/util/rebase.sh"`

- Run tests:
  `vagrant ssh -c ". /vagrant/util/test.sh"`

## Plugins

### vagrant-hostsupdater

This updates /etc/hosts for us as per config in the Vagrantfile. Only makes
changes on up/halt/destroy/reload type commands (not provision).

`vagrant plugin install vagrant-hostsupdater`

https://github.com/cogitatio/vagrant-hostsupdater

### vagrant-cachier

Caches apt-get downloads on the host so guest machines build much faster.

`vagrant plugin install vagrant-cachier`

http://fgrehm.viewdocs.io/vagrant-cachier

### sahara

Allows you to "sandbox" your vagrant VM. Essentially this means you can rollback
changes to the state of the VM while sandbox mode is on. This plugin works by
managing VM snapshots for you.

`vagrant plugin install sahara`

https://github.com/jedi4ever/sahara
