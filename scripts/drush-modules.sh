#!/usr/bin/env bash
#
# This installs extra drush utilities if they are missing.

drush dl drush-7.x-5.x-dev -y;

drush dl registry_rebuild -n;

# Install coder in the .drush directory so we have a standard for code sniffing.
drush dl coder-8.x-2.x-dev --destination=/home/vagrant/.drush -n;

# Pandarus uses a capital "P" which is bullshit on case sensitive systems.
drush dl Pandarus-7.x-1.0-rc1 -n;
if [ ! -d /usr/share/drush/commands/pandarus ]
  then
    mv /usr/share/drush/commands/Pandarus/ /usr/share/drush/commands/pandarus/;
fi
