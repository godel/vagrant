# Run tests for the project. This should work equally well for vagrant and CI.

# First we pull in config set for the project.
. /vagrant/util/config.sh;

cd $DRUPALDIR;

echo "Checking custom modules for coding standards:";
phpcs --standard=$CSSTANDARD --extensions=$CSEXTENSIONS --ignore=$CSIGNORE -p ./sites/all/modules/custom/;

echo "Checking custom themes for coding standards:";
phpcs --standard=$CSSTANDARD --extensions=$CSEXTENSIONS --ignore=$CSIGNORE -p ./sites/all/themes/custom/;

echo "Checking settings files for coding standards:";
phpcs --standard=$CSSTANDARD --extensions=$CSEXTENSIONS --ignore=$CSIGNORE -p ./sites/all/settings/;

./bin/behat --profile=$BEHATPROFILE --strict;

cd $CURRENTPATH;
