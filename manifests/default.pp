# System.
Exec { path => ["/usr/local/sbin/", "/usr/local/bin/", "/usr/sbin/", "/usr/bin/", "/sbin/", "/bin/", "/usr/games/" ] }

class {'apt': }
package {'make': }
package {'htop': }
package {'git': }

# Configure PHP.
class {'php':
  template => '/vagrant/conf/php.ini',
  require => Package['make'],
}

php::module{'mcrypt': }
php::module{'ldap': }
php::module {'gd': }
php::module {'mysql': }
php::module {'apc':
  module_prefix => "php-"
}
php::module {'curl': }
php::module {'imap': }
php::module {'memcache': }
php::pecl::module { "xhprof":
  use_package     => 'false',
  preferred_state => 'beta',
}
php::pear::module {'PHP_CodeSniffer':
  use_package => 'false',
}

include composer

# Configure Solr.
class {'solr':
  require => Package['git'],
}
->
exec {'ensure_solr_instance':
  command => 'sudo create-solr-instance vagrant 7 sapi',
}

# Configure memcache
class {'memcached':
  max_memory => 64,
}

# Configure MySQL.
include ::mysql::server
mysql::db {'vagrant':
  user => 'vagrant',
  password => 'vagrant',
  host => 'localhost',
  grant => ['ALL'],
}

# Add a second db for multisite testing.
include ::mysql::server
mysql::db {'two':
  user => 'vagrant',
  password => 'vagrant',
  host => 'localhost',
  grant => ['ALL'],
}

# Add a database for housing a remote db for migration testing.
include ::mysql::server
mysql::db {'migrate':
  user => 'vagrant',
  password => 'vagrant',
  host => 'localhost',
  grant => ['ALL'],
}

# Configure Apache.
class {'apache':
  default_vhost => false,
  # Required for mod PHP.
  mpm_module => 'prefork',
  default_confd_files => true,
  require => File['/drupal'],
}
# Add mod PHP.
include apache::mod::php
include apache::mod::rewrite
include apache::mod::ssl

# Create a default vhost for drupal in /drupal
apache::vhost { 'drupal':
  port    => '80',
  docroot => '/drupal',
  default_vhost => true,
# Allow .htaccess overrides.
  directories => [ { path => '/drupal', allow_override => ['All'] } ],
}

#Add xdebug
include xdebug
xdebug::config { 'default':
  remote_host => '10.0.2.2', # Vagrant users can specify their address
  remote_port => '9000', # Change default settings
  remote_enable => 1,
  remote_handler => 'dbgp',
}

package {'drush': }
file {'/home/vagrant/.drush':
  ensure => 'directory',
  owner => 'vagrant',
  group => 'vagrant',
}
->
file {'/home/vagrant/.drush/aliases.drushrc.php':
  ensure => file,
  source => '/drupal-dev/aliases.drushrc.php',
  owner => 'vagrant',
  group => 'vagrant',
}

# # Ensure docroot https://github.com/puphpet/puphpet/issues/206
file {'/drupal':
  ensure => 'directory',
}
->
file {'/drupal/sites':
  ensure => 'directory',
  owner => 'www-data',
  group => 'www-data',
}
->
file {'/drupal/sites/all':
  ensure => 'directory',
  owner => 'www-data',
  group => 'www-data',
}
->
file {['/drupal/sites/default', '/drupal/sites/vagrant.ld', '/drupal/sites/two.ld']:
  ensure => 'directory',
  owner => 'www-data',
  group => 'www-data',
}
->
file {['/drupal/sites/default/default.settings.php', '/drupal/sites/default/settings.php', '/drupal/sites/vagrant.ld/default.settings.php', '/drupal/sites/two.ld/default.settings.php']:
  ensure => 'file',
  source => '/vagrant/drupal/default.settings.php',
  owner => 'www-data',
  group => 'www-data',
}
->
file {['/drupal/sites/vagrant.ld/public', '/drupal/sites/two.ld/public']:
  ensure => 'directory',
  owner => 'www-data',
  group => 'www-data',
  # Set this to 775 to allow vagrant to sync files down, assuming vagrant user
  # is added to the www-data group elsewhere.
  mode => '0775',
  recurse => true,
}
->
file {['/drupal/sites/vagrant.ld/private', '/drupal/sites/vagrant.ld/private/temp', '/drupal/sites/two.ld/private', '/drupal/sites/two.ld/private/temp']:
  ensure => 'directory',
  owner => 'www-data',
  group => 'www-data',
}
# Linking the default files directory to the vagrant.ld one is a nice easy way
# to fix broken paths coming down from prod content, but is likely pretty
# unfriendly to multisite/complex setups. If you're handling a multisite setup
# you might want to review this.
->
file {'/drupal/sites/default/files':
  ensure => 'link',
  target => '/drupal/sites/vagrant.ld/public',
}
->
file {'/drupal/sites/default/private':
  ensure => 'link',
  target => '/drupal/sites/vagrant.ld/private',
}
->
file {'/drupal/sites/vagrant.ld/settings.php':
  ensure => 'file',
  source => '/vagrant/drupal/vagrant.settings.php',
  owner => 'www-data',
  group => 'www-data',
}
# Sets up a settings file for multisite, but not any files
->
file {'/drupal/sites/two.ld/settings.php':
  ensure => 'file',
  source => '/vagrant/drupal/two.settings.php',
  owner => 'www-data',
  group => 'www-data',
}