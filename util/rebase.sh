# WARNING:
# The rebase script will only work if you have ssh user agents working
# correctly. On a Mac OSX system, this is handled automatically if you have a
# passphrase on your SSH keys.
# @see http://www.phase2technology.com/blog/running-an-ssh-agent-with-vagrant/
# @see https://help.github.com/articles/working-with-ssh-key-passphrases#os-x-keychain

# Please try to keep this roughly in line with what drone.io does in ci.sh

# First we pull in config set for the project.
. /vagrant/util/config.sh;

###
# BELOW BE DRAGONS!!! (no touchie)
###

ME=`whoami`;
if [ "$ME" != "$SAFEUSER" ]
  then
    echo "current user: $ME";
    echo "You have tried to run this script as a user other than vagrant. I assume this means you are running it from OUTSIDE a vagrant box. You need to run this script from INSIDE a vagrant box.";
    return;
fi

cd $SITESDIR;

  # Get db.

if [ "$REMOTEALIAS" != "NONE" ]
  then
  echo "clearing remote cache.";
  drush $REMOTEALIAS cc all --strict=0;
fi

echo "backup local db.";
drush -y $LOCALALIAS sql-dump --result-file --gzip;

echo "pulling remote db.";
# Pantheon has its own weird sql sync thingo.
if [ "$HOSTINGPROVIDER" == "pantheon" ]
  then
    echo "Pantheon db sync.";
    drush -y $LOCALALIAS sql-drop;
    drush pan-sql-sync $REMOTEALIAS $LOCALALIAS -y;
fi

if [ "$HOSTINGPROVIDER" == "acquia" ]
  then
    echo "NOT Pantheon db sync.";
    drush -y sql-sync $REMOTEALIAS $LOCALALIAS --create-db;
fi

# Facilitate manual db backups.
if [ "$HOSTINGPROVIDER" == "manual" ]
  then
    echo "MANUAL db import";
    drush -y $LOCALALIAS sqlc < /vagrant/shared/database.sql;
fi

# We hit the vagrant front page to trigger Context Module.
curl -IL $LOCALURL;

# Freshen db.
echo "basic deploy tasks.";
drush $LOCALALIAS rr;
drush $LOCALALIAS updb -y;

# Features has an internal semaphore that can lead to endlessly rebuilding features
# if there was a fatal in a previous step.
drush $LOCALALIAS vdel -y --exact features_semaphore;
drush $LOCALALIAS fra -y;

# As there's no real way to determine whether features reverting or updb needs
# to happen first, we do both in case it is needed.
drush $LOCALALIAS updb -y;
drush $LOCALALIAS fra -y;

drush $LOCALALIAS cc all;

# We hit the vagrant front page to trigger Context Module.
curl -IL $LOCALURL;


if [ "$REMOTEALIAS" != "NONE" ]
  then
    # Remote file syncing does not work from within a Drupal directory due to some
    # issue with drush and Pantheon.
    echo "pulling files.";
    cd /
    drush -y rsync $REMOTEALIAS:%files/ $LOCALFILESDIR --mode=rvlz --size-only --no-p --no-g --progress --exclude="styles/";
    sudo chown -R www-data:www-data $LOCALFILESDIR;
    cd $SITESDIR;
fi

# @todo - refactor this into the above condition.
if [ "$REMOTEALIAS" == "NONE" ]
  then
    drush -y rsync /vagrant/shared/files/ $LOCALFILESDIR --mode=rvlz --size-only -no-p -no-g --progress --exclude="styles/";
    sudo chown -R www-data:www-data $LOCALFILESDIR;
    cd $SITESDIR;
fi

# Solr needs to be re-indexed after we pull the db in.
# echo "re-indexing solr.";
if [ "$SOLR" == "SOLR" ]
  then
    drush $LOCALALIAS solr-delete-index;
    drush $LOCALALIAS solr-mark-all;
    drush $LOCALALIAS solr-index;

    # Run cron.
    echo "running cron.";
    drush $LOCALALIAS elysia-cron;
fi

# Set CSS and JS preprocessing off.
echo "Disabling CSS/JS caching";
drush $LOCALALIAS vset preprocess_js 0;
drush $LOCALALIAS vset preprocess_css 0;

# Clear cache once more, for luck.
drush $LOCALALIAS cc all;

# One time login.
echo "generating one time login link.";
drush $LOCALALIAS uli;
cd $CURRENTDIR;
